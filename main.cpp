#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <climits>
#include <cmath>

int min(const std::vector<int> &seq) {
    int min = INT_MAX;
    for (int i: seq) {
        if (min > i) { min = i; }
    }
    return min;
}

int max(const std::vector<int> &seq) {
    int max = INT_MIN;
    for (int i: seq) {
        if (max < i) { max = i; }
    }
    return max;
}

int even_sum(const std::vector<int> &seq) {
    int sum = 0;
    for (int i: seq) {
        if (i % 2 == 0) {
            sum += i;
        }
    }
    return sum;
}

double poly_value(float point, const std::vector<double> &poly) {
    double value = 0;
    for (int i = 0; i <= poly.size(); ++i) {
        value += pow(point, i) * poly[i];
    }
    return value;
}


void sequence_part() {
    int n;
    std::cout << "Type in an amount of numbers in random sequence" << std::endl;
    std::cin >> n;

    std::vector<int> seq(n);
    for (int i = 0; i < n; ++i) {
        seq[i] = rand();
        std::cout << seq[i] << std::endl;
    }


    std::cout << "max: " << max(seq) << std::endl << "min: " << min(seq) << std::endl;
    std::cout << "sum of even elements of a sequence: " << even_sum(seq) << std::endl;
}


void poly_part() {
    int pow;
    std::cout << "Enter power of a polynomial: ";
    std::cin >> pow;
    std::vector<double> poly(pow);
    for (int i = 0; i <= pow; ++i) {
        std::cout << "Enter koef " << i << ": ";
        std::cin >> poly[i];
    }
    std::cout << poly_value(0.0, poly) << " " << poly_value(1.0, poly) << " " << poly_value(2.0, poly);
}


int main() {
    srand(time(nullptr));
    sequence_part();
    poly_part();
}
